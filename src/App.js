import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import ReactGA from 'react-ga';
import { connect } from 'react-redux';

import Layout from './hoc/Layout/Layout';
import Auth from './containers/enrollment/Auth/Auth';
import Logout from './containers/enrollment/Logout/Logout'
import * as actions from './store/actions';

import Worktimelist from './containers/worktime/worktime'

class App extends Component {
  
  componentWillMount() {
    this.props.onTryAutoSignup()
  }
  
  componentDidMount() {
    ReactGA.initialize('UA-126700247-1')
    this.props.history.listen(location => ReactGA.pageview(location.pathname));
  }
  
  render() {
    // call fetch here only dev
    let routes = (
      <Switch>
        <Route path="/auth" component={Auth}/>
        <Route path="/" component={Auth}/>
        <Redirect to="/auth" />
      </Switch>
    )

    if ( this.props.isAuthenticated ) {    
        
      routes = (
        <Switch>
          <Route path="/worktimelist" component={Worktimelist} />
          <Route path="/logout" component={Logout} />
          <Route path="/" component={Worktimelist} />
          <Redirect to="/worktimelist" />
        </Switch>
      )
    }

    return (
      <div >
        <Layout>
          {routes}
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {    
  return {
    isAuthenticated: state.auth.googleUser !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch( actions.authCheckState() )
  };
};

export default withRouter( connect( mapStateToProps, mapDispatchToProps )( App ) );
