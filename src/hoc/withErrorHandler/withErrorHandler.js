import React, { Component } from 'react';

import Modal from '../../components/Modal/Modal';
import Aux from '../Auxi/Auxi';


const withErrorHandler = ( WrappedComponent, axios ) => {
    return class extends Component {
        state = {
            error: null
        }

        UNSAFE_componentWillMount () {
            this.reqInterceptor = axios.interceptors.request.use( req => {
                this.setState( { error: null } );
                return req;
            } );
            this.resInterceptor = axios.interceptors.response.use( res => res, error => {
                this.setState( { error: error } );
            } );
        }

        componentWillUnmount () {
            axios.interceptors.request.eject( this.reqInterceptor );
            axios.interceptors.response.eject( this.resInterceptor );
        }

        errorConfirmedHandler = () => {
            this.setState( { error: null } );
        }

        render () {
            const err = this.state.error
            const detailedErrorMessage = (_error) => {
              console.log(_error)
                if (_error !== null && _error.response !== null) {
                    return (
                    <Aux>
                        <p>{_error.response.data.code}</p>
                        <p>{_error.response.data.message}</p>
                        <p>{JSON.stringify(_error.response.data.description)}</p>
                    </Aux> 
                    )
                }
                return null
            }
            return (
                <Aux>
                    <Modal
                        show={err}
                        modalClosed={this.errorConfirmedHandler}>
                        {err ? err.message : null}
                    { detailedErrorMessage(err) }
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Aux>
            );
        }
    }
}

export default withErrorHandler;