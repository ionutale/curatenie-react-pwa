/**
 * for subtracting days insert negative value
 */
Date.prototype.addDays = function(days) {
  const date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

/**
 * returns the first day of the current month
 */
Date.prototype.month1st = function() {
  const month = new Date().getMonth() + 1
  const year = new Date().getFullYear()
  return new Date(`${year}-${month}-01`).getTime();
}

/**
 * returns the first day of the current month or a date that is passed as parameter
 */
Date.prototype.prevMonth1st = function(yDate=null) {
  let yourDate = yDate || new Date()
  let month = yourDate.getMonth() - 1 < 0 ? 12 : yourDate.getMonth() - 1
  return new Date(yourDate.getFullYear(), month, 1).getTime();
}

/**
 * @param {*: Number} monthNumber 
 * it returns a string with the full name of the month in english
 */
Date.prototype.monthNameLong = function(monthNumber) {
  return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][monthNumber];
}

/**
 * 
 * @param {*: Number} monthNumber 
 * returns a string with 3-4 letters of the month in english
 */
Date.prototype.monthNameShort = function(monthNumber) {
  return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'][monthNumber];
}

/**
 * 
 * @param {*: Number} millis 
 * returns the date as a String with format "yyyy-MM-dd"
 */
Date.prototype.millisToDateString = function(millis) {
  const date = new Date(millis)
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  let day = date.getDate()
  day = day < 10 ? `0${day}` : day

  return `${year}-${month}-${day}`;
}

/**
 * 
 * @param {*: Number} millis 
 * returns the date as a String with format "HH:mm"
 */
Date.prototype.millisToTimeString = function(millis) {
  const d = new Date(millis)
  let hour = d.getHours()
  hour = hour < 10 ? `0${hour}` : hour
  let minutes = d.getMinutes()
  minutes = minutes < 10 ? `0${minutes}` : minutes
  
  return `${hour}:${minutes}`;
}

/**
 * 
 * @param {date: String, time: String} date 
 * the string must be of format date: 'yyyy-MM-dd' 
 * the string must be of format time: 'HH:mm' 
 * returns millis
 */

Date.prototype.fromStringToEpochDate = function(date, time) {
  return new Date(`${date}T${time}`);
}