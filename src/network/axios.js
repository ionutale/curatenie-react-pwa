import axios from 'axios'
const URL = {
    'dev': 'http://localhost:3100/wtr',
    'qa': 'http://ec2-54-93-231-82.eu-central-1.compute.amazonaws.com:3100/wtr',
}
const token = localStorage.getItem('token')
const xUserId = localStorage.getItem('xUserId')
console.log('xUserId:', xUserId);

const instance = axios.create({
    baseURL: process.env.NODE_ENV === 'production' ? URL.qa : URL.dev,
    headers: {
        common: {
                'Authorization': `Bearer ${token}`,
                'x-userid': xUserId
        }
    } 
})

export default instance 