import axios from './axios'
const WORKTIME_URL = '/worktimerecords'

export const fetchWorktimesFromServer = async (fromDate, toDate) => {
    return await axios.get(`${WORKTIME_URL}?startTime=${fromDate}&endTime=${toDate}`)
}

export const getWorktime = async plateNumber => {
    return await axios.get(`${WORKTIME_URL}/${plateNumber}`)
}

export const pushNewWorktimeToServer = async (worktime) => {
    return await axios.post(WORKTIME_URL, worktime)
}

export const updateWorktime = async (worktime, id) => {
    return await axios.put(`${WORKTIME_URL}/${id}`, worktime)
} 

export const deleteWorktime = async (worktime) => {
    return await axios.delete(`${WORKTIME_URL}/${worktime._id}`)
}

/**
 * this structure is correct
 * the return promise is what we want 
 * this way we will handle the errors in the controller
 * this way (again) we can show a banner with some mapped errors
 */
