import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    worktimes: [],
    worktime: {},
    loading: false,
};

const newWorktimeStart = ( state ) => {
    return updateObject( state, { loading: true } );
};

const newWorktimeSuccess = ( state, action ) => {
    const newWorktime = updateObject( action.worktimeData, { id: state.worktimes.length } );
    state.worktimes.unshift( newWorktime ) 
    return updateObject( state, {
        loading: false,
        purchased: true,
        worktimes: state.worktimes
    } );
};

const newWorktimeFail = ( state ) => {
    return updateObject( state, { loading: false } );
};

/************************* UPDATE */

const updateWorktimeStart = ( state) => {
    return updateObject( state, { loading: true } );
};

const updateWorktimeSuccess = ( state, action ) => {
    const updateWorktime = action.worktimeData
    
    let list = [...state.worktimes]
    const index = list.findIndex(item => item._id === updateWorktime._id)
    list.splice(index, 1, updateWorktime ) 

    return updateObject( state, {
        loading: false,
        purchased: true,
        worktimes: list,
        worktime: updateWorktime
    } );
};

const updateWorktimeFail = ( state ) => {
    return updateObject( state, { loading: false } );
};

/*********************** DELETE */

const deleteWorktimeStart = ( state) => {
    return updateObject( state, { loading: true } );
};

const deleteWorktimeSuccess = ( state, action ) => {
    const deleteWorktime = {...action.worktimeData}
    
    let list = [...state.worktimes]
    const index = list.findIndex(item => item._id === deleteWorktime._id)
    
    console.log("before splice",list.length);
    list.splice(index, 1 ) 
    console.log("after splice",list.length);

    return updateObject( state, {
        loading: false,
        worktimes: list,
        worktime: deleteWorktime
    } );
};

const deleteWorktimeFail = ( state ) => {
    return updateObject( state, { loading: false } );
};

/******************* FETCH all  */


const fetchWorktimesStart = ( state ) => {
    return updateObject( state, { loading: true } );
};

const fetchWorktimesSuccess = ( state, action ) => {
    return updateObject( state, {
        worktimes: action.worktimes,
        loading: false
    } );
};

const fetchWorktimesFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};


/******************* FETCH by plate number  */
const fetchWorktimeStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchWorktimeSuccess = ( state, action ) => {
    return updateObject( state, {
        worktime: action.worktime,
        loading: false
    } );
};

const fetchWorktimeFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};


/************** Export Js */
const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {

        case actionTypes.NEW_WORKTIME_START: return newWorktimeStart( state, action );
        case actionTypes.NEW_WORKTIME_SUCCESS: return newWorktimeSuccess( state, action )
        case actionTypes.NEW_WORKTIME_FAIL: return newWorktimeFail( state, action );

        case actionTypes.UPDATE_WORKTIME_START: return updateWorktimeStart( state, action );
        case actionTypes.UPDATE_WORKTIME_SUCCESS: return updateWorktimeSuccess( state, action )
        case actionTypes.UPDATE_WORKTIME_FAIL: return updateWorktimeFail( state, action );

        case actionTypes.DELETE_WORKTIME_START: return deleteWorktimeStart( state, action );
        case actionTypes.DELETE_WORKTIME_SUCCESS: return deleteWorktimeSuccess( state, action )
        case actionTypes.DELETE_WORKTIME_FAIL: return deleteWorktimeFail( state, action );

        case actionTypes.FETCH_WORKTIME_START: return fetchWorktimeStart( state, action );
        case actionTypes.FETCH_WORKTIME_SUCCESS: return fetchWorktimeSuccess( state, action );
        case actionTypes.FETCH_WORKTIME_FAIL: return fetchWorktimeFail( state, action );

        case actionTypes.FETCH_WORKTIMES_START: return fetchWorktimesStart( state, action );
        case actionTypes.FETCH_WORKTIMES_SUCCESS: return fetchWorktimesSuccess( state, action );
        case actionTypes.FETCH_WORKTIMES_FAIL: return fetchWorktimesFail( state, action );
        default: return state;
    }
};

export default reducer;