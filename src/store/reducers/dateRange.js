import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const FROM_LAST_DATE = new Date(localStorage.getItem('FROM_LAST_DATE') || new Date());
const TO_LAST_DATE = new Date(localStorage.getItem('TO_LAST_DATE') || new Date());


const initialState = {
  fromDate: FROM_LAST_DATE !== 0 ? FROM_LAST_DATE : new Date(),
  toDate: TO_LAST_DATE !== 0 ? TO_LAST_DATE : new Date(),
}

const fromDate = (state, fromDate) => {
	localStorage.setItem('FROM_LAST_DATE', fromDate.fromDate);
  return updateObject(state, fromDate);
}

const toDate = (state, toDate) => {
	localStorage.setItem('TO_LAST_DATE', toDate.toDate);
  return updateObject(state, toDate);
}

const reducer = (state = initialState, action) => {
	
  switch (action.type) {
    case actionTypes.FROM_DATE: return fromDate(state, action);
    case actionTypes.TO_DATE: return toDate(state, action);
    default: return state;
  }
}

export default reducer;
