import axios from  '../../network/axios'
import * as actionTypes from './actionTypes'
/* eslint no-unused-vars: 0 */  // --> OFF

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (googleUser) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        googleUser: googleUser
    }
}

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const logout = () => {
    localStorage.removeItem('googleUser');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const checkAuthTimeout = (expirationTime) => {     
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime);
    };
};

export const auth = (googleUser) => {
    return dispatch => {
        dispatch(authStart());
        
        localStorage.setItem('googleUser', JSON.stringify({ ...googleUser }));
        localStorage.setItem('token', googleUser.id_token);
        localStorage.setItem('xUserId', googleUser.email);

        axios.defaults.headers.common['Authorization'] = `Bearer ${googleUser.id_token}`;
        axios.defaults.headers.common['xUserId'] = googleUser.email;
        
        dispatch(authSuccess(googleUser));
        dispatch(checkAuthTimeout((googleUser.expires_at - Date.now())));
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};

export const authCheckState = () => {
    return dispatch => {
        const googleUser = JSON.parse(localStorage.getItem('googleUser'));
             
        if (!googleUser) {
            dispatch(logout());
        } else {
            if (googleUser.expires_at <= new Date()) {
                dispatch(logout());
            } else {
                dispatch(authSuccess(googleUser));
                dispatch(checkAuthTimeout((googleUser.expires_at - Date.now())));
            }   
        }
    };
};
