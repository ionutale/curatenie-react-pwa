export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const SET_AUTH_REDIRECT_PATH = 'SET_AUTH_REDIRECT_PATH';

/** WORKTIME */
export const NEW_WORKTIME_START = 'NEW_WORKTIME_START';
export const NEW_WORKTIME_SUCCESS = 'NEW_WORKTIME_SUCCESS';
export const NEW_WORKTIME_FAIL = 'NEW_WORKTIME_FAIL';
export const NEW_WORKTIME_INIT = 'NEW_WORKTIME_INIT';

export const UPDATE_WORKTIME_START = 'UPDATE_WORKTIME_START';
export const UPDATE_WORKTIME_SUCCESS = 'UPDATE_WORKTIME_SUCCESS';
export const UPDATE_WORKTIME_FAIL = 'UPDATE_WORKTIME_FAIL';
export const UPDATE_WORKTIME_INIT = 'UPDATE_WORKTIME_INIT';

export const DELETE_WORKTIME_START = 'DELETE_WORKTIME_START';
export const DELETE_WORKTIME_SUCCESS = 'DELETE_WORKTIME_SUCCESS';
export const DELETE_WORKTIME_FAIL = 'DELETE_WORKTIME_FAIL';
export const DELETE_WORKTIME_INIT = 'DELETE_WORKTIME_INIT';

export const FETCH_WORKTIME_REQUEST = 'FETCH_WORKTIME_REQUEST';
export const FETCH_WORKTIME_START = 'FETCH_WORKTIME_START';
export const FETCH_WORKTIME_SUCCESS = 'FETCH_WORKTIME_SUCCESS';
export const FETCH_WORKTIME_FAIL = 'FETCH_WORKTIME_FAIL';

export const FETCH_WORKTIMES_REQUEST = 'FETCH_WORKTIMES_REQUEST';
export const FETCH_WORKTIMES_START = 'FETCH_WORKTIMES_START';
export const FETCH_WORKTIMES_SUCCESS = 'FETCH_WORKTIMES_SUCCESS';
export const FETCH_WORKTIMES_FAIL = 'FETCH_WORKTIMES_FAIL';

/**
 * Date 
 */

export const FROM_DATE = 'FROM_DATE';
export const TO_DATE = 'TO_DATE';
