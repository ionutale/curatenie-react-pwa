import * as actionTypes from './actionTypes';
import * as axiosWorktime from '../../network/axios-worktime';

export const newWorktimeSuccess = (id, worktimeData) => {
  return {
    type: actionTypes.NEW_WORKTIME_SUCCESS,
    worktimeId: id,
    worktimeData: worktimeData,
  };
};

export const newWorktimeFail = error => {
  return {
    type: actionTypes.NEW_WORKTIME_FAIL,
    error: error
  };
}

export const newWorktimeStart = () => {
  return {
    type: actionTypes.NEW_WORKTIME_START
  };
};

export const newWorktime = (worktimeData) => {
  return async dispatch => {
    dispatch(newWorktimeStart());
    try {
      const res = await axiosWorktime.pushNewWorktimeToServer(worktimeData)
      dispatch(newWorktimeSuccess(res.data._id, res.data));
    } catch (error) {
      dispatch(newWorktimeFail(error));
    }
  };
};

export const newWorktimeInit = () => {
  return {
    type: actionTypes.NEW_WORKTIME_INIT
  };
};

/******************************** FETCH */

export const fetchWorktimesSuccess = (worktimes) => {
  return {
    type: actionTypes.FETCH_WORKTIMES_SUCCESS,
    worktimes: worktimes
  };
};

export const fetchWorktimesFail = (error) => {
  return {
    type: actionTypes.FETCH_WORKTIMES_FAIL,
    error: error
  };
};

export const fetchWorktimesStart = () => {
  return {
    type: actionTypes.FETCH_WORKTIMES_START
  };
};

export const fetchWorktimes = (fromDate, toDate) => {

  return async dispatch => {
    dispatch(fetchWorktimesStart());
    try {
      const res = await axiosWorktime.fetchWorktimesFromServer(fromDate.toISOString(), toDate.toISOString())
      dispatch(fetchWorktimesSuccess(res.data));
    } catch (error) {
      fetchWorktimesFail(error)
    }
  };
};

/******************************** FETCH */

export const fetchWorktimeSuccess = (worktime) => {
  return {
    type: actionTypes.FETCH_WORKTIME_SUCCESS,
    worktime: worktime
  };
};

export const fetchWorktimeFail = (error) => {
  return {
    type: actionTypes.FETCH_WORKTIME_FAIL,
    error: error
  };
};

export const fetchWorktimeStart = () => {
  return {
    type: actionTypes.FETCH_WORKTIME_START
  };
};

export const fetchWorktime = (plateNumber) => {
  return async dispatch => {
    try {
      dispatch(fetchWorktimeStart());
      const res = await axiosWorktime.getWorktime(plateNumber)
      const wtrs = res.data[0] || {}
      dispatch(fetchWorktimeSuccess(wtrs.sort((a,b) => a.startTime - b.startTime))) // just send an empty object, this will avoid crashing app
    } catch (error) {
      fetchWorktimeFail(error)
    }
  };
};

/****************************** UPDATE */

export const updateWorktimeSuccess = (id, worktimeData) => {
  return {
    type: actionTypes.UPDATE_WORKTIME_SUCCESS,
    worktimeId: id,
    worktimeData: worktimeData,
  };
};

export const updateWorktimeFail = (error) => {
  return {
    type: actionTypes.UPDATE_WORKTIME_FAIL,
    error: error
  };
}

export const updateWorktimeStart = () => {
  return {
    type: actionTypes.UPDATE_WORKTIME_START
  };
};

export const updateWorktime = (worktimeData, id) => {
  return async dispatch => {
    dispatch(updateWorktimeStart());
    try {
      const res = await axiosWorktime.updateWorktime(worktimeData, id)
      dispatch(updateWorktimeSuccess(res.data._id, res.data));
    } catch (error) {
      dispatch(updateWorktimeFail(error));
    }
  };
};

/********************* DELETE */

export const deleteWorktimeSuccess = (id, worktimeData) => {
  return {
    type: actionTypes.DELETE_WORKTIME_SUCCESS,
    worktimeId: id,
    worktimeData: worktimeData,
  };
};

export const deleteWorktimeFail = (error) => {
  return {
    type: actionTypes.DELETE_WORKTIME_FAIL,
    error: error
  };
}

export const deleteWorktimeStart = () => {
  return {
    type: actionTypes.DELETE_WORKTIME_START
  };
};

export const deleteWorktime = (worktimeData) => {

  return async dispatch => {
    dispatch(deleteWorktimeStart());
    try {
      const res = await axiosWorktime.deleteWorktime(worktimeData)
      dispatch(deleteWorktimeSuccess(res.data.deleted, worktimeData));
    } catch (error) {
      dispatch(deleteWorktimeFail(error));
    }
  };
};
