import * as actionTypes from './actionTypes'


export const fromDate = (date) => {
  return {
    type: actionTypes.FROM_DATE,
    fromDate: date
  }
}

export const toDate = (date) => {
  return {
    type: actionTypes.TO_DATE,
    toDate: date
  }
}
