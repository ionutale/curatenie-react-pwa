import React, {Component} from 'react';
import { connect } from 'react-redux';

import css from './Toolbar.module.css';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';
import Aux from '../../hoc/Auxi/Auxi';

class Toolbar extends Component {

    render () {
        const googleUser = this.props.googleUser
        let loggedUser = <h2 className={css.LoggedUser}>Loggin</h2>
        if (googleUser) {
            loggedUser = (
                <div >
                    <div className={css.LoggedUser}>
                        <img className={css.ProfileImage} src={googleUser.imageUrl}/>
                        <h3 className={css.UserName}>{googleUser.name}</h3>
                    </div>
                </div>
                )
        }
        return ( 
            <Aux>

                { this.props.loading ? <div className={css.Loading}> </div> : null }
                <header className={css.Toolbar}>
                    <DrawerToggle clicked={this.props.drawerToggleClicked} /> 
                    <h2 className={css.ProjName}>Curatenie </h2>
                    { loggedUser }
                    
                </header>
                <p>dateISO: {new Date().toISOString()}</p>
                <p>dateUTC: {new Date().toUTCString()}</p>
                <p>dateGMT: {new Date().toGMTString()}</p>
                <p>date string: {Date()}</p>
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.worktime.loading,
        googleUser: state.auth.googleUser
    }
}

export default connect(mapStateToProps)(Toolbar);