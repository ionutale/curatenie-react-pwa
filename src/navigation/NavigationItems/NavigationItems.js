import React from 'react';

import css from './NavigationItems.module.css';
import NavigationItem from './NavigationItem/NavigationItem';
import Aux from '../../hoc/Auxi/Auxi';

const NavigationItems = ( props ) => {
    let navigationItems = (
        <Aux>
            <NavigationItem link="/worktimelist">Lista Ore</NavigationItem>
            <NavigationItem link="/logout">Logout</NavigationItem>
        </Aux>
    )
    return (
    <ul className={css.NavigationItems}>
        <NavigationItem link="/" exact>Grafice</NavigationItem>
        {props.isAuthenticated ? 
            navigationItems : 
            <NavigationItem link="/auth">Authenticate</NavigationItem>}
    </ul>
)}

export default NavigationItems;
