import React, { Component } from 'react'
import css from './worktime.module.css'

import { connect } from 'react-redux'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import * as actions from '../../store/actions'
import axios from "../../network/axios";
import WorktimeCell from './worktimeCell/worktimeCell';
import StatusBar from '../statusBar/statusBar';
import Aux from '../../hoc/Auxi/Auxi';


const LATEST_WTR = JSON.parse(localStorage.getItem('latestWTR') || '{}')
const CREATE_NEW_WTR = {
  "description": LATEST_WTR.description || 'Add a description',
  "billed": false,
  "startTime": new Date(LATEST_WTR.startTime || Date.now()),
  "endTime": new Date(LATEST_WTR.endTime || Date.now()),
  "user": LATEST_WTR.user || '',
  "projectId": "projectId",
  "projectName": LATEST_WTR.projectName || 'pulizia scale'
}

let timer = null

class Worktime extends Component {

  state = {
    fromDate: this.props.fromDate,
    toDate: this.props.toDate,
    worktimes: this.props.worktimes,
  }

  fetchData = () => {
    timer = setTimeout(() => this.props.onFetchWorktimes(this.props.fromDate, this.props.toDate), 1000)
  }

  async componentDidMount() {
    this.fetchData()
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if ((nextProps.fromDate !== prevState.fromDate) || (nextProps.toDate !== prevState.toDate)) {
      return { fromDate: nextProps.fromDate, toDate: nextProps.toDate, }
    } else
      return null
  }

  componentDidUpdate(prevProps, prevState) {

    if ((this.state.fromDate !== prevState.fromDate) || (this.state.toDate !== prevState.toDate)) {
      clearTimeout(timer);
      this.fetchData()
    }
  }

  calculateHours = (workingTimeArr) => {
    if (workingTimeArr.length === 0) return '0'

    const totalwtr = workingTimeArr.map(item => {
      return (new Date(item.endTime) - new Date(item.startTime))
    })
    console.log("totalwtr:", totalwtr)
    const total = totalwtr.reduce((acumulator = 0, initialValue = 0) => {
      return acumulator + initialValue
    })
    console.log("total:", total)
    return total;
  }

  msToTime(duration) {
    let milliseconds = parseInt((duration % 1000) / 100)
    let seconds = Math.floor((duration / 1000) % 60)
    let minutes = Math.floor((duration / (1000 * 60)) % 60)
    let hours = Math.floor((duration / (1000 * 60 * 60)));
    console.log(duration, hours)

    hours = (hours < 10) ? `0${hours}` : hours;
    minutes = (minutes < 10) ? `0${minutes}` : minutes;
    seconds = (seconds < 10) ? `0${seconds}` : seconds;
    console.log(hours)
    return `${hours}:${minutes}:${seconds}.${milliseconds}`
  }


  render() {
    let month = new Date().getMonth()
    const wtrArr = this.props.worktimes
      .sort((a, b) => {
        return b.startTime - a.startTime
      })
      .map(item => {
        const cMonth = new Date(item.startTime).getMonth()
        if (month !== cMonth) {
          month = cMonth
          return (
            <Aux key={item._id}>
              <h3 key={item.startTime} className={css.MonthNameHeader}>{new Date().monthNameLong(month)}</h3>
              <WorktimeCell worktime={item}></WorktimeCell>
            </Aux>
          )
        }
        return <WorktimeCell key={item._id} worktime={item}></WorktimeCell>
      })

    return (
      <div>
        <StatusBar totalWorkedTime={this.msToTime(this.calculateHours(this.props.worktimes))}></StatusBar>
        <WorktimeCell key={CREATE_NEW_WTR._id} worktime={CREATE_NEW_WTR} createNew={true}></WorktimeCell>
        {wtrArr}
      </div>
    )
  }
}


const mapStateToProps = state => {
  return {
    worktimes: state.worktime.worktimes,
    loading: state.worktime.loading,
    fromDate: state.dateRange.fromDate,
    toDate: state.dateRange.toDate,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchWorktimes: (fromDate, toDate) => dispatch(actions.fetchWorktimes(fromDate, toDate)),
    onFromDate: () => dispatch(actions.fromDate()),
    onToDate: () => dispatch(actions.toDate()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Worktime, axios));
