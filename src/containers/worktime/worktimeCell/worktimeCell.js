import React, { Component } from 'react';
import css from './worktimeCell.module.css';
import Rowcell from '../../../components/rowcell/rowcell';

import { connect } from 'react-redux'
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import * as actions from '../../../store/actions'
import axios from "../../../network/axios";

class WorktimeCell extends Component {

    state = {
        worktime: {
          ...this.props.worktime,
          projectId: (this.props.worktime.projectId === "" ? "defaultProjectId" : this.props.worktime.projectId), 
          projectName: (this.props.worktime.projectName === "" ? "project name" : this.props.worktime.projectName),
          startTime: new Date(this.props.worktime.startTime),
          endTime: new Date(this.props.worktime.endTime),
        },
        updatable: false,
        toBeCreated: this.props.createNew,
        debug: {
          strTime: "",
          aDate: ""
        }
    }
    constructor(props) {
        super(props);
    }
    
    extractDate = epochDate => {
        const date = new Date(epochDate)
        const year = date.getFullYear()
        let month = date.getMonth() + 1
        month = month < 10 ? `0${month}` : month
        let day = date.getDate()
        day = day < 10 ? `0${day}` : day

        return `${year}-${month}-${day}`;
    }

		// is not importing the time as utc so i loose 1h
		// fix this part
    extractTime = aDate => {
      const date = new Date(aDate)
     alert(JSON.stringify({debug: {
        ...this.state.debug,
        aaDate: aDate,
        extractTime: date
      }}))

      let hour = date.getHours()
      hour = hour < 10 ? `0${hour}` : `${hour}`
      let minutes = date.getMinutes()
      minutes = minutes < 10 ? `0${minutes}` : `${minutes}`

      return `${hour}:${minutes}`;
	}
		
    extractTimeUTC = aDate => {
        const date = new Date(aDate)
        console.log("aDate: ", aDate, date)
        let hour = date.getUTCHours()
        hour = hour < 10 ? `0${hour}` : `${hour}`
        let minutes = date.getMinutes()
        minutes = minutes < 10 ? `0${minutes}` : `${minutes}`

        return `${hour}:${minutes}`
    }

    fromStringToDate = (date, time) => new Date(`${date}T${time}`);

    onChange = (e) => {
        const wtr = this.state.worktime

        if (e.target.type === 'date') {
					const startTime = this.extractTime(wtr.startTime)
					const endTime = this.extractTime(wtr.endTime)

					const startDateTime = this.fromStringToDate(e.target.value, startTime)
					const endDateTime = this.fromStringToDate(e.target.value, endTime)
					
          this.setTimeRange(startDateTime, endDateTime)
        } else

        if (e.target.type === 'time') {            
            const strTime = e.target.value
            const date = this.extractDate(wtr.startTime)
            const aDate = new Date(`${date}T${strTime}Z`)           
            this.setState({debug: {
                strTime: strTime,
                aDate: aDate
            }})
            this.setWTR(e.target.name, aDate)
        } else 

        if (e.target.type === 'text') {
            this.setWTR(e.target.name, e.target.value)
        } else {
            console.log('unknow input type')
        }
        console.log(this.state.worktime);
        
    }

    setTimeRange = (startTime, endTime) => {
      this.setState({updatable: true})
      const wtr = {
        ...this.state.worktime,
        startTime,
        endTime
      }
      this.setState({ worktime: wtr })        
    }

    setWTR = (pName, value) => {
        this.setState({updatable: true})
        const wtr = {
            ...this.state.worktime,
            [pName]: value
        }
        this.setState({ worktime: wtr })        
    }

    onCancel = () => {

        this.setState({updatable: false})
    }

    onSave = () => {
        const {description, projectName, projectId, billed, startTime, endTime} = this.state.worktime
        const toUpdateWtr = {
            description, projectName, projectId, billed, startTime, endTime
        }
        this.props.onNewWorktime(toUpdateWtr)
        localStorage.setItem('latestWTR', JSON.stringify(toUpdateWtr))
    }

    onUpdate = () => {
        const {description, projectName, projectId, billed, startTime, endTime} = this.state.worktime
        const toUpdateWtr = {
            description, projectName, projectId, billed, startTime, endTime
        }
        this.props.onUpdateWorktime(toUpdateWtr, this.state.worktime._id)
        // this is a bug. when updating you need a callback to know if is actualy updated. if not, 
        // don't hide the buttons
        this.setState({updatable: false})
        localStorage.setItem('latestWTR', JSON.stringify(toUpdateWtr))
    }

    onDelete = () => {
        this.props.onDeleteWorktime(this.state.worktime)
    }

    render() {
        const workTime = this.state.worktime
        const updateWTRSection = (
            <section className={css.InfoContainer}>
                <button onClick={this.onCancel}>Cancel</button>
                <button onClick={this.onUpdate}>Update</button>
            </section>
        );
        const addNewWTRSection = (
            <section className={css.InfoContainer}>
                <button onClick={this.onSave}>Add</button>
            </section>
				)

        return (
            <Rowcell>
                {this.state.toBeCreated ? 
                <header>Create New Work-Time-Record</header> : 
                <button className={css.DeleteButton} onClick={this.onDelete}>x</button>}
                <p>{JSON.stringify(this.state.debug)}</p>
                <div className={css.InfoContainer}>
                    <div className={css.Column1}>
                        <div>
                            <label htmlFor="date">Date</label>
                            <input type="date" name="date" value={this.extractDate(workTime.startTime)} onChange={this.onChange} />
                        </div>
                        <div>
                            <label htmlFor="startTime">Start Time</label>
                            <input type="time" name="startTime" value={this.extractTime(workTime.startTime)} onChange={this.onChange} />
                        </div>
                        <div>
                            <label htmlFor="endTime">End Time</label>
                            <input type="time" name="endTime" value={this.extractTime(workTime.endTime)} onChange={this.onChange} />
                        </div>
                    </div>
                    <div className={css.Column2}>
                        <div>
                            <label htmlFor="projectName">Project Name</label>
                            <input type="text" name="projectName" value={workTime.projectName} onChange={this.onChange} />
                        </div>
                        <div>
                            <label htmlFor="description">Description</label>
                            <input type="text" name="description" value={workTime.description} onChange={this.onChange} />
                        </div>
												<div>
                            <label>Total </label>
							              <h3>{this.extractTimeUTC(workTime.endTime - workTime.startTime)}</h3>
                        </div>
                    </div>
                </div>
                {this.state.toBeCreated ? addNewWTRSection : updateWTRSection}
            </Rowcell>

        )
    }
}


const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        //worktime: state.worktime.worktime,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onUpdateWorktime: (updateWTR, id) => dispatch(actions.updateWorktime(updateWTR, id)),
        onDeleteWorktime: (deleteWTR) => dispatch(actions.deleteWorktime(deleteWTR)),
        onNewWorktime: (createWTR) => dispatch(actions.newWorktime(createWTR)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (withErrorHandler(WorktimeCell, axios));

