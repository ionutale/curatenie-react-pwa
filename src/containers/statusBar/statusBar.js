import React, { Component } from 'react';
import css from './statusBar.module.css';
import * as actions from '../../store/actions'
import { connect } from 'react-redux'


class StatusBar extends Component {
  state = {
    fromDate: this.props.fromDate,
    toDate: this.props.toDate
  }

  onChange = (e) => {
    const date = new Date(e.target.value)
    this.setWTR(e.target.name, date)
  }

  setWTR = (pName, value) => {
    this.setState({ [pName]: value })
    if (pName === 'fromDate') {
      this.props.onFromDate(value)
    }
    else {
      this.props.onToDate(value)
    }
  }


  render() {
    return (
      <section className={css.StatusBar}>
        <div>
          <label htmlFor="Din">Din:</label>
          <input type="date" name="fromDate" disabled={this.props.loading} value={new Date().millisToDateString(this.state.fromDate)} onChange={this.onChange} ></input>
        </div>
        <div>
          <label htmlFor="Pana:">Pana:</label>
          <input type="date" name="toDate" disabled={this.props.loading} value={new Date().millisToDateString(this.state.toDate)} onChange={this.onChange} ></input>
        </div>
        <h3>total: {this.props.totalWorkedTime}</h3>

      </section>
    )
  }
}


const mapStateToProps = state => {
  return {
    fromDate: state.dateRange.fromDate,
    toDate: state.dateRange.toDate,
    loading: state.worktime.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFromDate: (fromDate) => dispatch(actions.fromDate(fromDate)),
    onToDate: (toDate) => dispatch(actions.toDate(toDate)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StatusBar);
