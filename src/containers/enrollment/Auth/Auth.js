import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import css from './Auth.module.css'
import { GoogleLogin } from 'react-google-login';

import * as actions from '../../../store/actions';
import { debug } from 'util';

class Auth extends Component {
    state = {
        user: {},
        error: {}
    }

    componentDidMount() {
        if (this.props.authRedirectPath !== '/') {
            this.props.onSetAuthRedirectPath();
        }        
    }
    
    onSignIn = (googleUser) => {
        const id_token = googleUser.getAuthResponse().id_token;    
        const user = {
            ...googleUser.profileObj,
            ...googleUser.tokenObj,
            session_state: googleUser.tokenObj.session_state.extraQueryParams.authuser,
            id_token: id_token
        }

        console.log('user data ', user); 
        this.setState({'user': user});
        this.props.authSuccess(user)
    }

    onFailure = (error) => {
        this.setState({error: error});
    }

    render () {
        let authRedirect = null;

        if (this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath}/>
        }

        const profileData = (user) => {
            const keys = Object.keys(user);
            return keys.map(k => {
                return <div key={k}>{k} :<p>{user[k]}</p></div>
            })
        }
        return (
            <section className={css.Auth}>
            <h1>Please login with google in order to use the service.</h1>
            {authRedirect}
            <GoogleLogin
                clientId="106974451683-0hqj0rd17a1d4vq2m7gglm2mnsgkvmmi.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={this.onSignIn}
                onFailure={this.onFailure}
            />                
            <header>{profileData(this.state.user)}</header>
            <header>{profileData(this.state.error)}</header>
            </section>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.googleUser !== null,
        authRedirectPath: state.auth.authRedirectPath
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authSuccess: (googleUser) => dispatch(actions.auth(googleUser)),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( Auth );
